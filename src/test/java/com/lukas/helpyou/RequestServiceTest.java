package com.lukas.helpyou;

import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.lukas.helpyou.configuration.AppConfig;
import com.lukas.helpyou.enumeration.RequestStatus;
import com.lukas.helpyou.enumeration.RequestType;
import com.lukas.helpyou.enumeration.Role;
import com.lukas.helpyou.model.Request;
import com.lukas.helpyou.model.User;
import com.lukas.helpyou.service.RequestService;
import com.lukas.helpyou.service.UserService;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = AppConfig.class)
@Transactional
@Rollback
public class RequestServiceTest {
	
	@Autowired
    private UserService userService;
	
	@Autowired
    private RequestService requestService;
	
	private User user;
	private Request request;
	
	private User createUser() {
        User user = new User();
        user.setPassword("test_pass");
        user.setReqType(RequestType.COMPUTER);
        user.setRole(Role.CLIENT);
        user.setUsername("test username");
        return user;
    }
	
	private Request createRequest() {
        Request request = new Request();
        request.setDesc("desc");
        request.setRequestStatus(RequestStatus.ACCEPTED);
        request.setRequestType(RequestType.COMPUTER);
        request.setTitle("test title");
        request.setUserId(user.getId());
        return request;
    }

	@Before
	public void before() {
		User user = createUser();
		this.user = userService.addUser(user);
		Assert.assertNotNull(user);
		
		Request request = createRequest();
		this.request = requestService.addRequest(request);
		Assert.assertNotNull(this.request);
	}
	
	@Test
	public void testGet() {
		request = requestService.getRequest(request.getId());
		Assert.assertNotNull(request);
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testDelete() {
		requestService.deleteRequest(request.getId());
		requestService.getRequest(request.getId());
	}
	
	@Test
	public void testUpdate() {
		String title = "updated title";
		request.setTitle(title);
		Request request = requestService.updateRequest(this.request);
		Assert.assertEquals(title, request.getTitle());
	}
	
	@Test
    public void testGetRequestsByUser() {
        List<Request> requestsByUser = requestService.getClientRequests(user.getId());
        Assert.assertEquals(1, requestsByUser.size());
    }

}
