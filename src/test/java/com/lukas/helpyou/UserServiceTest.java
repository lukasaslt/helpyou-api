package com.lukas.helpyou;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.lukas.helpyou.configuration.AppConfig;
import com.lukas.helpyou.enumeration.RequestType;
import com.lukas.helpyou.enumeration.Role;
import com.lukas.helpyou.model.User;
import com.lukas.helpyou.service.UserService;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = AppConfig.class)
@Transactional
@Rollback
public class UserServiceTest {
	
	@Autowired
    private UserService userService;
	
	@Test
    public void testRegister() {
	    User user = new User();
        user.setId(1L);
        user.setPassword("test_pass");
        user.setReqType(RequestType.COMPUTER);
        user.setRole(Role.CLIENT);
        user.setUsername("test username");
        user = userService.addUser(user);
        
        User retrievedUser = userService.getUser(user.getId());
        Assert.assertNotNull(retrievedUser);
    }
	
	

}
