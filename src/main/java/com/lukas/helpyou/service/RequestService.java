package com.lukas.helpyou.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lukas.helpyou.model.Request;
import com.lukas.helpyou.model.User;
import com.lukas.helpyou.repository.RequestRepository;
import com.lukas.helpyou.repository.UserRepository;

@Service
@Transactional
public class RequestService {
	
	@Autowired
	private RequestRepository requestRepository;
	
	@Autowired
    private UserRepository userRepository;
	
	public Request addRequest(Request request) {
		return requestRepository.save(request);
	}
	
	public Request getRequest(Long id) {
		return requestRepository.findById(id).get();
	}
	
	public void deleteRequest(Long id) {
		requestRepository.deleteById(id);
	}
	
	public Request updateRequest(Request request) {
		return requestRepository.save(request);
	}
	
	public List<Request> getClientRequests(Long userId) {
        return requestRepository.findAllByUserId(userId);
    }
	
	public List<Request> getFixerRequests(Long userId) {
	    User user = userRepository.findById(userId).get();
        List<Request> userRequests = requestRepository.findAllByType(user.getReqType());        
        
        return userRequests.stream().filter(e -> 
            e.getAcceptedUserId() == null || e.getAcceptedUserId() == userId
        ).collect(Collectors.toList());
    }

}
