package com.lukas.helpyou.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lukas.helpyou.exception.HttpConflictException;
import com.lukas.helpyou.exception.HttpUnauthorizedException;
import com.lukas.helpyou.model.User;
import com.lukas.helpyou.repository.UserRepository;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public User addUser(User user) {
	    User foundUser = userRepository.findByUsername(user.getUsername());
	    
	    if (foundUser != null)
	        throw new HttpConflictException();
	    
	    user.setPassword(passwordEncoder.encode(user.getPassword()));
	    
		return userRepository.save(user);
	}
	
	public User getUser(Long id) {
		return userRepository.findById(id).get();
	}
	
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}
	
	public User updateUser(User user) {
		return userRepository.save(user);
	}
	
	public User login(User user) {
	    User foundUser = userRepository.findByUsername(user.getUsername());
	    
	    if (foundUser == null || !passwordEncoder.matches(user.getPassword(), foundUser.getPassword()))
	        throw new HttpUnauthorizedException();
	    
        return foundUser;
    }

}
