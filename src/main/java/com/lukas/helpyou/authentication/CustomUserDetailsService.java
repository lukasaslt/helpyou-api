package com.lukas.helpyou.authentication;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lukas.helpyou.model.User;
import com.lukas.helpyou.repository.UserRepository;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(email);
        
        if (user == null) {
            throw new UsernameNotFoundException("No user found with username: " + email);
        }
        
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), 
                Arrays.asList(new SimpleGrantedAuthority(user.getRole().toString())));
    }
}
