package com.lukas.helpyou.enumeration;

public enum RequestStatus {
	CREATED, ACCEPTED, COMPLETED
}
