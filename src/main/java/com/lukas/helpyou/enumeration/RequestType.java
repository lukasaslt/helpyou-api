package com.lukas.helpyou.enumeration;

public enum RequestType {
	ELECTRICITY, PLUMBING, COMPUTER, OTHER
}
