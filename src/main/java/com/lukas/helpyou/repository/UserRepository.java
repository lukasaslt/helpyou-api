package com.lukas.helpyou.repository;

import org.springframework.data.repository.CrudRepository;

import com.lukas.helpyou.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    
    User findByUsername(String username);
    
}
