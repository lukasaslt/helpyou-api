package com.lukas.helpyou.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lukas.helpyou.enumeration.RequestType;
import com.lukas.helpyou.model.Request;

public interface RequestRepository extends CrudRepository<Request, Long> {
    
    @Query(value = "SELECT r FROM Request r WHERE userId = ?1")
    List<Request> findAllByUserId(Long userId);
    
    @Query(value = "SELECT r FROM Request r WHERE requestType = ?1")
    List<Request> findAllByType(RequestType requestType);
    
}
