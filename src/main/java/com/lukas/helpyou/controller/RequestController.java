package com.lukas.helpyou.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lukas.helpyou.model.Request;
import com.lukas.helpyou.service.RequestService;

@RestController
@RequestMapping(value = "/request")
public class RequestController {
	
	@Autowired
	private RequestService requestService;
	
	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('CLIENT')")
	public void addRequest(@RequestBody Request request) {
		requestService.addRequest(request);
	}
	
	@RequestMapping(value = "/client/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('CLIENT')")
    public List<Request> getClientRequests(@PathVariable Long id) {
        return requestService.getClientRequests(id);
    }
	
	@RequestMapping(value = "/fixer/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('FIXER')")
    public List<Request> getFixerRequests(@PathVariable Long id) {
        return requestService.getFixerRequests(id);
    }
	
	@PreAuthorize("hasAuthority('CLIENT')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRequest(@PathVariable Long id) {
        requestService.deleteRequest(id);
    }
	
	@PreAuthorize("hasAuthority('FIXER')")
	@RequestMapping(method = RequestMethod.PUT)
    public Request updateRequest(@RequestBody Request request) {
        return requestService.updateRequest(request);
    }

}
