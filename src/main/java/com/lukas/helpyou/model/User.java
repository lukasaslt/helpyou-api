package com.lukas.helpyou.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.lukas.helpyou.enumeration.RequestType;
import com.lukas.helpyou.enumeration.Role;

import lombok.Data;

@Entity
@Table(name = "user")
@Data
public class User {
	
	@Id
    @Column(name = "id", unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "username", nullable = false)
	private String username;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "role", nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@Column(name = "request_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private RequestType reqType;

}
