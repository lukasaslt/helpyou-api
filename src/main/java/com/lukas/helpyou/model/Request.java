package com.lukas.helpyou.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.lukas.helpyou.enumeration.RequestStatus;
import com.lukas.helpyou.enumeration.RequestType;

import lombok.Data;

@Entity
@Table(name = "request")
@Data
public class Request {
	
	@Id
    @Column(name = "id", unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "title", nullable = true)
	private String title;
	
	@Column(name = "description", nullable = true)
	private String desc;
	
	@Column(name = "type", nullable = false)
	@Enumerated(EnumType.STRING)
	private RequestType requestType;
	
	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private RequestStatus requestStatus;
	
	@Column(name = "user_id", nullable = false)
	private Long userId;
	
	@Column(name = "accepted_user_id", nullable = true)
	private Long acceptedUserId;

}
